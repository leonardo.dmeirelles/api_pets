import mongoose from 'mongoose';

const { Schema } = mongoose;

export interface Pets {
    pet_name: string;
    pet_age: number;
    pet_gender: 'male' | 'female';
}

export const petsSchema = new Schema<Pets>({
    pet_name: {
        type: String,
        required: true,
    },

    pet_age: {
        type: Number,
        required: true,
    },

    pet_gender: {
        type: String,
        required: true,
    },
});
