import mongoose from 'mongoose';
import { petsSchema, Pets } from './pets-schemas';

const { Schema } = mongoose;

interface Hosting {
    tutor_name: string;
    pets: Pets[];
    hostingStart: Date;
    hostingEnd: Date;
}

const hostingSchema = new Schema<Hosting>(
    {
        tutor_name: {
            type: String,
            required: true,
        },

        pets: [petsSchema],

        hostingStart: {
            type: Date,
        },

        hostingEnd: {
            type: Date,
        },
    },

    {
        timestamps: true,
    }
);

export default hostingSchema;
