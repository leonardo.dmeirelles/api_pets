import mongoose from 'mongoose';

const { DB_CONNECTION }: any = process.env;

const connection = () => {
    console.log(DB_CONNECTION);

    return mongoose.createConnection(DB_CONNECTION);
};

export default connection;
