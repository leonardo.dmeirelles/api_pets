import mongoose from 'mongoose';
import connection from '../connection';
import hostingSchema from '../schemas/hosting-schema';

const { Schema } = mongoose;

const db = connection();

const partnerSchemaDb = new Schema(
    {
        name: {
            type: String,
            required: true,
            minlength: 3,
        },

        email: {
            type: String,
            unique: true,
            required: true,
        },

        password: {
            type: String,
            required: true,
            select: false,
        },

        phone: {
            type: String,
            required: true,
            unique: true,
        },

        isActive: {
            type: Boolean,
            required: true,
        },

        hosting: [hostingSchema],
    },

    {
        timestamps: true,
        toObject: { useProjection: true },
        toJSON: { useProjection: true },
    }
);

const PartnerModel = db.model('partners', partnerSchemaDb);

export default PartnerModel;
