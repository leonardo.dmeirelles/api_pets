import mongoose from 'mongoose';
import connection from '../connection';
import { petsSchema } from '../schemas/pets-schemas';

const { Schema } = mongoose;

const db = connection();

const userSchemaDb = new Schema(
    {
        name: {
            type: String,
            required: true,
            minlength: 3,
        },

        email: {
            type: String,
            unique: true,
            required: true,
        },

        password: {
            type: String,
            required: true,
            select: false,
        },

        phone: {
            type: String,
            required: true,
            unique: true,
        },

        isActive: {
            type: Boolean,
            required: true,
        },

        pets: [petsSchema],
    },

    {
        timestamps: true,
        toObject: { useProjection: true },
        toJSON: { useProjection: true },
    }
);

const UserModel = db.model('users', userSchemaDb);

export default UserModel;
