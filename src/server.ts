import express from 'express';
import cors from 'cors';
import router from './routers/index';

const api = express();

api.use(cors());
api.use(express.json());

api.use(router);

const port: number = 5000;

api.listen(port, () => {
    console.log(`Api running on port ${port}`);
});
