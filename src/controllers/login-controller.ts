import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import UserModel from '../database/models/user-model';

interface User {
    _id: string;
    name: string;
    email: string;
    password?: string;
    phone: string;
}

const { JWT_SECRET, JWT_EXPIRES_TIME_IN_SECS }: any = process.env;

// Generates a JWT token using user information from the database, for the login process!
const generatePayload = (user: any) => {
    const userPayload: User = {
        _id: user.id,
        name: user.name,
        email: user.email,
        phone: user.phone,
    };
    const token = jwt.sign(userPayload, JWT_SECRET, {
        expiresIn: Number.parseInt(JWT_EXPIRES_TIME_IN_SECS, 10),
    });

    return { token };
};

// Verify if the user has valid credentials, and responds with a JWT token!
const loginController = async (req: Request, res: Response) => {
    const { email, password } = req.body;

    const user: any = await UserModel.findOne({ email }, { _id: 1, name: 1, email: 1, password: 1, phone: 1 });

    if (!user) {
        return res.status(401).json({ message: 'Wrong credentials' });
    }

    const passwordCompare = await bcrypt.compare(password, user.password);

    if (!passwordCompare) {
        return res.status(401).json({ message: 'Wrong credentials' });
    }

    const payload = generatePayload(user);

    return res.status(200).json(payload);
};

export default loginController;
