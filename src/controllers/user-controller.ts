import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import UserModel from '../database/models/user-model';

const { BCRYPT_SALT_ROUNDS }: any = process.env;

// Gets all the users in the database!
export const getUsersController = async (req: Request, res: Response) => {
    try {
        const users = await UserModel.find();

        res.status(200).json(users);
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error',
            error: err,
        });
    }
};

// Gets one user by its id in the database!
export const getUserByIdController = async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
        const user = await UserModel.findById(id);
        if (!user) {
            res.status(404).json({
                message: 'Not found',
                error: `User with ID ${id} is not in the database`,
            });
        } else {
            res.json(user);
        }
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error',
            error: err,
        });
    }
};

// Post one new user in the database!
export const postUserController = async (req: Request, res: Response) => {
    const { body } = req;

    const saltRounds = await bcrypt.genSalt(Number(BCRYPT_SALT_ROUNDS));

    const passwordHash = await bcrypt.hash(body.password, saltRounds);

    try {
        const user = new UserModel({ ...body, password: passwordHash });

        await user.save();

        res.status(201).json(user);
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error',
            error: err,
        });
    }
};

// Updates some key in the user model!
export const patchUserController = async (req: Request, res: Response) => {
    const { body } = req;
    const { id } = req.params;

    try {
        const updatedUser = await UserModel.findByIdAndUpdate(id, { $set: body }, { new: true });

        if (!updatedUser) {
            res.status(404).json({
                message: 'Not found',
                error: `User with ID ${id} is not in the database`,
            });
        } else {
            res.json(updatedUser);
        }
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error',
            error: err,
        });
    }
};

// Delete one user by its ID in the database!
export const deleteUserByIdController = async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
        const deletedUser = await UserModel.findByIdAndDelete(id);

        if (!deletedUser) {
            res.status(404).json({
                message: 'Not found',
                error: `User with ID ${id} is not in the database`,
            });
        } else {
            res.status(200).json({ message: 'User deleted successfully' });
        }
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error',
            error: err,
        });
    }
};
