import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';

const { JWT_SECRET }: any = process.env;

const authenticationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    let message;

    const { authorization }: any = req.headers;

    if (!authorization) {
        message = 'No token provided';
    }

    const authParts: string[] = authorization.split('');

    if (authParts?.length !== 2) {
        message = 'Wrong token format';
    }

    const [schema, token] = authParts;

    if (schema.toLowerCase() !== 'bearer') {
        message = 'Wrong token schema';
    }

    try {
        // const decodedToken = jwt.verify(token, JWT_SECRET);
        // req.user = decodedToken;
        jwt.verify(token, JWT_SECRET);
        return next();
    } catch (error: any) {
        if (error.message === 'TokenExpiredError') {
            message = 'Access token expired';
        }
        message = 'User not provided or unauthorized';
    }

    return res.status(401).json({ message });
};

export default authenticationMiddleware;
