import { Router } from 'express';
import {
    getUsersController,
    getUserByIdController,
    postUserController,
    deleteUserByIdController,
    patchUserController,
} from '../../controllers/user-controller';

// Assign each user route to its corresponding middleware, schema and controller!
const assignUserRoutes = (router: Router) => {
    const USERS_URL = '/users';

    router.get(`${USERS_URL}`, getUsersController);

    router.get(`${USERS_URL}/:id`, getUserByIdController);

    router.post(`${USERS_URL}`, postUserController);

    router.delete(`${USERS_URL}/:id`, deleteUserByIdController);

    router.patch(`${USERS_URL}/:id`, patchUserController);
};

export default assignUserRoutes;
