import { Router } from 'express';
import loginController from '../../controllers/login-controller';

const assignAuthenticationRoutes = (router: Router) => {
    router.post('/login', loginController);
};

export default assignAuthenticationRoutes;
