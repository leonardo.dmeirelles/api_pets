import express, { Router } from 'express';
import assignAuthenticationRoutes from './auth-routes/assign-auth-routes';
import assignUserRoutes from './user-routes/assign-user-routes';

const router: Router = express.Router();

assignUserRoutes(router);

assignAuthenticationRoutes(router);

export default router;
